antigrav (0.0.3-8) unstable; urgency=low

  * QA upload.
  * Declare compliance with Debian Policy 4.2.1
  * Change maintainer to QA group
  * Update vcs git link to salsa
  * Bump debhelper to version 11
  * Add hardening flags (Closes: #900899)

 -- Matheus Faria <matheus.sousa.faria@gmail.com>  Thu, 01 Nov 2018 16:07:39 -0300

antigrav (0.0.3-7) unstable; urgency=medium

  * Team upload.
  * wrap-and-sort -sa.
  * Use compat level 9 and require debhelper >= 9.
  * Vcs-Browser: Use https.
  * Declare compliance with Debian Policy 3.9.8.
  * Change Homepage field to https://tracker.debian.org/pkg/antigrav
    because the original homepage is gone.
  * antigravitaattori.desktop: Add keywords and a comment in German.
  * debian/watch: Stop watching upstream's website. It is gone.
  * Drop dirs file. Not needed.
  * Add install file and install antigrav.png and desktop file.
    (Closes: #726206)
  * Add DEP-3 header to all patches.
  * Add format-not-a-string-literal.patch and fix FTBFS due to
    -Werror=format-security flag.
  * Override dh_clean and ensure that antigrav can be built twice in a row.

 -- Markus Koschany <apo@debian.org>  Wed, 13 Apr 2016 09:16:04 +0200

antigrav (0.0.3-6.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix for libpng16 (Closes: #741901)

 -- Tobias Frost <tobi@debian.org>  Tue, 19 Jan 2016 08:01:54 +0100

antigrav (0.0.3-6) unstable; urgency=low

  [ Evgeni Golov ]
  * Correct Vcs-* URLs to point to anonscm.debian.org

  [ Vincent Fourmond ]
  * Use dh-autoreconf to get rid of build problems on new architectures
    (closes: #752014)
  * Bump to recent standards, no need for changes
  * Now using system tinyxml (and not the embedded copy)
  * Refreshed patches

 -- Vincent Fourmond <fourmond@debian.org>  Tue, 02 Sep 2014 22:49:32 +0200

antigrav (0.0.3-5) unstable; urgency=low

  * Build-depends on libpng-dev (closes: #662269)
  * Conforms to standards 3.9.3

 -- Vincent Fourmond <fourmond@debian.org>  Mon, 05 Mar 2012 21:33:21 +0100

antigrav (0.0.3-4) unstable; urgency=low

  * Upload to unstable

 -- Vincent Fourmond <fourmond@debian.org>  Wed, 09 Feb 2011 21:06:24 +0100

antigrav (0.0.3-3) experimental; urgency=low

  * Switched to format 3.0 (quilt)
  * Applied patch courtesy of Mario Lang <mlang@debian.org> to fix few
    DACA-detected problems (closes: #610305) (03_daca_fixes.diff)
  * Adding ${misc:Depends} for potential dh-induced dependencies
  * Switched build system to a pure dh 7 debian/rules (and, in passing, I
    cannot help but express my gratitude to the authors of the dh sequencer)
  * Conforms to standards 3.9.1
  * Patch 04-height-selection.diff to provide a -H option to allow the
    user to change the aspect ratio (closes: #538910)

 -- Vincent Fourmond <fourmond@debian.org>  Wed, 26 Jan 2011 22:34:32 +0100

antigrav (0.0.3-2) unstable; urgency=low

  * Push back the 01-fix-png-load to fix segfault on (at least) AMD64
    (Closes: #457944)
  * Tidy up patches
  * Added myself to Uploaders

 -- Vincent Fourmond <fourmond@debian.org>  Thu, 27 Dec 2007 15:11:07 +0100

antigrav (0.0.3-1) unstable; urgency=low

  [ Barry deFreese ]
  * New maintainer (Closes: #453739)
    + Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
  * Add myself to uploaders
  * New upstream release
  * Bump standards version to 3.7.3 (No changes necessary)
  * Move Homepage field from package description to header
  * Add VCS fields to control
  * Make distclean not ignore errors
  * Remove deprecated Encoding tag from desktop file
  * Add quilt patching system and move previous changes to patches:
    + 02_fix_sound.diff - Fix sound handling
  * Add watch file
  * Change menu section from Games/Arcade to Games/Action

 -- Barry deFreese <bddebian@comcast.net>  Tue, 11 Dec 2007 12:35:27 -0500

antigrav (0.0.2-6) unstable; urgency=low

  * Orphaning package, setting maintainer to the Debian QA Group.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Wed, 05 Dec 2007 15:49:55 +0100

antigrav (0.0.2-5) unstable; urgency=low

  * Apply patch from Michael Daenzer <daenzer@debian.org>
    to fix sound. (Closes: #399758)
  * Added desktop file.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sat, 30 Jun 2007 14:56:31 +0200

antigrav (0.0.2-4) unstable; urgency=low

  * Applied patch from Pascal Giard <evilynux@gmail.com> 
    to fix PNG image loading. (Closes: #400051)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Fri,  1 Dec 2006 09:19:53 +0100

antigrav (0.0.2-3) unstable; urgency=low

  * Allow playing without sound. (Closes: #399758)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Thu, 30 Nov 2006 09:09:46 +0100

antigrav (0.0.2-2) unstable; urgency=low

  * Applied patch of Andreas Jochens to fix FTBFS. (Closes: #398248)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 19 Nov 2006 20:46:01 +0100

antigrav (0.0.2-1) unstable; urgency=low

  * Initial release. (Closes: #393265)

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 15 Oct 2006 21:21:30 +0200
